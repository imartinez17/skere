# Explica amb les teves paraules les diferències generals entre la web 1.0, 2.0 i 3.0. Digues alguns exemples de cadascuna

La web 1.0 es de solo lectura. Nadie podia editarlo excepto el webmaster con lo que si el webmaster no lo editaba la web no se actualizaba

El objetivo de la web 2.0 es compartir el conocimiento. Permite a los usuarios interactuar con otros usuarios o cambiar contenido de la web, cosa que en la web 1.0 no se podía hacer.

La web 3.0 ya no solo permite conectarse con los ordenadores, también nos deja conectarnos desde tablets, moviles incluso desde las televisiones.
